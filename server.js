const express = require("express");
const Unsplash = require("unsplash-js").default;
const toJson = require("unsplash-js").toJson;
global.fetch = require("node-fetch");
require("./config");

const APPLICATION_ID = process.env.APPLICATION_ID;
const SECRET_KEY = process.env.SECRET_KEY;

const app = express();

const HOST = "0.0.0.0";
const PORT = 5000;

app.use(express.json({ extended: false }));

const unsplash = new Unsplash({ accessKey: APPLICATION_ID });

app.get("/api/photos", async (req, res) => {
  try {
    if (!req.query.search) {
      let data = await unsplash.photos.listPhotos(
        req.query.start,
        req.query.count
      );
      let json = await toJson(data);
      res.json(json);
    } else {
      let re = await unsplash.search.photos(
        req.query.search,
        req.query.start,
        req.query.count
      );
      let jso = await toJson(re);
      res.json(jso.results);
    }
  } catch (e) {
    console.log(e);
  }
});

app.listen(PORT, HOST);
console.log(`Server is running on ${HOST}:${PORT}...`);
